package com.alice.poker.service.impl;

import com.alice.poker.model.User;
import com.alice.poker.service.PokerService;
import com.google.common.collect.ImmutableList;

public class PokerServiceImplTest {

    public static void main(String[] args) {
        PokerService pokerService = new PokerServiceImpl();
        User botUser1 = new User("Bot 1", 1000D, true);
        User botUser2 = new User("Bot 2", 1000D, true);
        User humanUser = new User("Yogesh", 1000D, false);
        pokerService.play(ImmutableList.of(botUser1, botUser2, humanUser));
    }
}
