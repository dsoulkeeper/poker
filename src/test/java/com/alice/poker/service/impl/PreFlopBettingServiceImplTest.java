package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.model.Rank;
import com.alice.poker.model.Suit;
import com.alice.poker.model.User;
import com.google.common.collect.ImmutableList;

public class PreFlopBettingServiceImplTest {

    private PreFlopBettingService preFlopPreFlopBettingService = new PreFlopBettingService();
    private PokerHand pokerHand;

    public void setup() {
        User botUser = new User("Bot", 1000D, true);
        Card botHoleCard1 = new Card(1, Suit.CLUB, Rank.KING, true);
        Card botHoleCard2 = new Card(2, Suit.DIAMOND, Rank.KING, true);
        PlayerPokerHand bot = new PlayerPokerHand(botUser);
        bot.setHoleCards(ImmutableList.of(botHoleCard1, botHoleCard2));

        User humanUser = new User("Yogesh", 1000D, false);
        Card humanHoleCard1 = new Card(3, Suit.CLUB, Rank.ACE, true);
        Card humanHoleCard2 = new Card(4, Suit.DIAMOND, Rank.ACE, true);

        PlayerPokerHand human = new PlayerPokerHand(humanUser);
        human.setHoleCards(ImmutableList.of(humanHoleCard1, humanHoleCard2));

        pokerHand = new PokerHand();
        pokerHand.setPlayerPokerHands(ImmutableList.of(bot, human));
    }

    public boolean shouldCorrectlyPerformPreflopBetting() {
        preFlopPreFlopBettingService.collectBets(pokerHand);
        boolean bettingRoundFinished = preFlopPreFlopBettingService.isBettingRoundFinished(pokerHand);
        while (!bettingRoundFinished) {
            bettingRoundFinished = shouldCorrectlyPerformPreflopBetting();
        }
        System.out.println("Round finished.");
        return true;
    }

    public static void main(String[] args) {
        PreFlopBettingServiceImplTest test = new PreFlopBettingServiceImplTest();
        test.setup();
        test.shouldCorrectlyPerformPreflopBetting();
    }
}
