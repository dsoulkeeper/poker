package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.model.Rank;
import com.alice.poker.model.Suit;
import com.alice.poker.model.User;
import com.alice.poker.service.PokerHandRecognizerService;
import com.alice.poker.service.PokerHandWeightCalculatorService;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.alice.poker.model.HandType.STRAIGHT;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class WinnerDeterminerServiceImplTest {

    private final PokerHandWeightCalculatorService pokerHandWeightCalculatorService = new PokerHandWeightCalculatorServiceImpl();
    private final PokerHandRecognizerService pokerHandRecognizerService = new PokerHandRecognizerServiceImpl(pokerHandWeightCalculatorService);
    private final WinnerDeterminerServiceImpl winnerDeterminerService = new WinnerDeterminerServiceImpl(pokerHandRecognizerService);

    private PokerHand pokerHand;
    private List<Card> player1Cards = new ArrayList<>(), player2Cards = new ArrayList<>();

    @Before
    public void setup() {
        // Arrange
        User user1 = new User("Yogesh", 1000D, false);
        Card user1HoleCard1 = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card user1HoleCard2 = new Card(2, Suit.CLUB, Rank.KING, false);
        List<Card> user1HoleCards = ImmutableList.of(user1HoleCard1, user1HoleCard2);

        User user2 = new User("Ankita", 1000D, false);
        Card user2HoleCard1 = new Card(3, Suit.CLUB, Rank.QUEEN, false);
        Card user2HoleCard2 = new Card(4, Suit.SPADE, Rank.QUEEN, false);
        List<Card> user2HoleCards = ImmutableList.of(user2HoleCard1, user2HoleCard2);

        //flop
        Card flopCard1 = new Card(5, Suit.DIAMOND, Rank.QUEEN, false);
        Card flopCard2 = new Card(6, Suit.HEART, Rank.JACK, false);
        Card flopCard3 = new Card(7, Suit.SPADE, Rank.TWO, false);

        //turn
        Card turnCard = new Card(8, Suit.HEART, Rank.SEVEN, false);

        //river
        Card riverCard = new Card(9, Suit.DIAMOND, Rank.TEN, false);


        PlayerPokerHand player1PokerHand = new PlayerPokerHand(user1);
        player1PokerHand.setHoleCards(user1HoleCards);
        PlayerPokerHand player2PokerHand = new PlayerPokerHand(user2);
        player2PokerHand.setHoleCards(user2HoleCards);

        pokerHand = new PokerHand();
        pokerHand.setFlopCards(ImmutableList.of(flopCard1, flopCard2, flopCard3));
        pokerHand.setTurnCard(turnCard);
        pokerHand.setRiverCard(riverCard);
        pokerHand.setPlayerPokerHands(ImmutableList.of(player1PokerHand, player2PokerHand));

        player1Cards.addAll(pokerHand.getCommunityCards());
        player1Cards.addAll(user1HoleCards);

        player2Cards.addAll(pokerHand.getCommunityCards());
        player2Cards.addAll(user2HoleCards);
    }

    @Test
    public void shouldDetermineCorrectWinnerBetweenTwoPlayers() {
        // Act
        PlayerPokerHand winner = winnerDeterminerService.determinerWinner(pokerHand);
        // Assert
        assertEquals(winner.getHandType(), STRAIGHT);
    }
}
