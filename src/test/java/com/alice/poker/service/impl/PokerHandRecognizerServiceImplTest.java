package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.HandType;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.Rank;
import com.alice.poker.model.Suit;
import com.alice.poker.model.User;
import com.alice.poker.service.PokerHandWeightCalculatorService;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;

import static com.alice.poker.model.HandType.FLUSH;
import static com.alice.poker.model.HandType.FOUR_OF_A_KIND;
import static com.alice.poker.model.HandType.FULL_HOUSE;
import static com.alice.poker.model.HandType.HIGH_CARD;
import static com.alice.poker.model.HandType.ONE_PAIR;
import static com.alice.poker.model.HandType.STRAIGHT;
import static com.alice.poker.model.HandType.STRAIGHT_FLUSH;
import static com.alice.poker.model.HandType.THREE_OF_A_KIND;
import static com.alice.poker.model.HandType.TWO_PAIRS;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class PokerHandRecognizerServiceImplTest {

    private PokerHandWeightCalculatorService pokerHandWeightCalculatorService = new PokerHandWeightCalculatorServiceImpl();
    private PokerHandRecognizerServiceImpl pokerHandRecognizerService = new PokerHandRecognizerServiceImpl(pokerHandWeightCalculatorService);

    @Test
    public void shouldCorrectlyDetermineSpecialIncrementalCaseOfOneToFive() {
        // Arrange
        Card cardTwo = new Card(1, Suit.CLUB, Rank.TWO, false);
        Card cardAce = new Card(2, Suit.CLUB, Rank.ACE, false);
        Card cardFive = new Card(3, Suit.CLUB, Rank.FIVE, false);
        Card cardFour = new Card(4, Suit.CLUB, Rank.FOUR, false);
        Card cardThree = new Card(5, Suit.CLUB, Rank.THREE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(cardTwo, cardAce, cardFive, cardFour, cardThree);
        boolean result = pokerHandRecognizerService.isStraight(cards);

        // Assert
        assertTrue(result);
    }

    @Test
    public void shouldCorrectlyDetermineFaceCardSequence() {
        // Arrange
        Card cardKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card cardAce = new Card(2, Suit.CLUB, Rank.ACE, false);
        Card cardQueen = new Card(3, Suit.CLUB, Rank.QUEEN, false);
        Card cardJack = new Card(4, Suit.DIAMOND, Rank.JACK, false);
        Card cardTen = new Card(5, Suit.CLUB, Rank.TEN, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(cardKing, cardAce, cardQueen, cardJack, cardTen);
        boolean result = pokerHandRecognizerService.isStraight(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);

        // Assert
        assertTrue(result);
        assertEquals(STRAIGHT, recognize);
    }

    @Test
    public void shouldFalseForIncorrectSequence() {
        // Arrange
        Card cardKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card cardAce = new Card(2, Suit.CLUB, Rank.ACE, false);
        Card cardQueen = new Card(3, Suit.CLUB, Rank.QUEEN, false);
        Card cardJack = new Card(4, Suit.CLUB, Rank.JACK, false);
        Card cardTwo = new Card(5, Suit.CLUB, Rank.TWO, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(cardKing, cardAce, cardQueen, cardJack, cardTwo);
        boolean result = pokerHandRecognizerService.isStraight(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);

        // Assert
        assertFalse(result);
        assertEquals(FLUSH, recognize);
    }

    @Test
    public void shouldReturnTrueForAceToFiveStraightFlush() {
        // Arrange
        Card cardTwo = new Card(1, Suit.CLUB, Rank.TWO, false);
        Card cardAce = new Card(2, Suit.CLUB, Rank.ACE, false);
        Card cardFive = new Card(3, Suit.CLUB, Rank.FIVE, false);
        Card cardFour = new Card(4, Suit.CLUB, Rank.FOUR, false);
        Card cardThree = new Card(5, Suit.CLUB, Rank.THREE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(cardTwo, cardAce, cardFive, cardFour, cardThree);
        boolean result = pokerHandRecognizerService.isStraightFlush(cards);

        // Assert
        assertTrue(result);
    }

    @Test
    public void shouldReturnTrueForRoyalFlush() {
        // Arrange
        Card cardKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card cardAce = new Card(2, Suit.CLUB, Rank.ACE, false);
        Card cardQueen = new Card(3, Suit.CLUB, Rank.QUEEN, false);
        Card cardJack = new Card(4, Suit.CLUB, Rank.JACK, false);
        Card cardTen = new Card(5, Suit.CLUB, Rank.TEN, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(cardKing, cardAce, cardQueen, cardJack, cardTen);
        boolean result = pokerHandRecognizerService.isStraightFlush(cards);

        HandType recognize = pokerHandRecognizerService.recognize(cards);

        // Assert
        assertTrue(result);
        assertEquals(STRAIGHT_FLUSH, recognize);
    }

    @Test
    public void shouldReturnFalseWhenCardsAreStraightButNotFlush() {
        // Arrange
        Card cardTwo = new Card(1, Suit.CLUB, Rank.TWO, false);
        Card cardAce = new Card(2, Suit.CLUB, Rank.ACE, false);
        Card cardFive = new Card(3, Suit.DIAMOND, Rank.FIVE, false);
        Card cardFour = new Card(4, Suit.CLUB, Rank.FOUR, false);
        Card cardThree = new Card(5, Suit.CLUB, Rank.THREE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(cardTwo, cardAce, cardFive, cardFour, cardThree);
        boolean result = pokerHandRecognizerService.isStraightFlush(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldReturnFalseWhenCardsAreFlushButNotStraight() {
        // Arrange
        Card cardKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card cardAce = new Card(2, Suit.CLUB, Rank.ACE, false);
        Card cardQueen = new Card(3, Suit.CLUB, Rank.QUEEN, false);
        Card cardJack = new Card(4, Suit.CLUB, Rank.JACK, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(cardKing, cardAce, cardQueen, cardJack, cardNine);
        boolean result = pokerHandRecognizerService.isStraightFlush(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueForValidFourOfAKind() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeKing = new Card(3, Suit.SPADE, Rank.KING, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeKing, heartKing, cardNine);
        boolean result = pokerHandRecognizerService.isFourOfAKind(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);

        // Assert
        assertTrue(result);
        assertEquals(FOUR_OF_A_KIND, recognize);
    }

    @Test
    public void shouldReturnFalseForInValidFourOfAKind() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeTwo, heartKing, cardNine);
        boolean result = pokerHandRecognizerService.isFourOfAKind(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueForValidFullHouse() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeKing = new Card(3, Suit.SPADE, Rank.KING, false);
        Card heartNine = new Card(4, Suit.HEART, Rank.NINE, false);
        Card clubNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeKing, heartNine, clubNine);
        boolean result = pokerHandRecognizerService.isFullHouse(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);

        // Assert
        assertTrue(result);
        assertEquals(FULL_HOUSE, recognize);
    }

    @Test
    public void shouldReturnFalseForInValidFullHouse() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeTwo, heartKing, cardNine);
        boolean result = pokerHandRecognizerService.isFullHouse(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueForValidThreeOfAKind() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeTwo, heartKing, cardNine);
        boolean result = pokerHandRecognizerService.isThreeOfAKind(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);
        // Assert
        assertTrue(result);
        assertEquals(THREE_OF_A_KIND, recognize);
    }

    @Test
    public void shouldReturnFalseForInValidThreeOfAKind() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeKing = new Card(3, Suit.SPADE, Rank.KING, false);
        Card heartNine = new Card(4, Suit.HEART, Rank.NINE, false);
        Card clubNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeKing, heartNine, clubNine);
        boolean result = pokerHandRecognizerService.isThreeOfAKind(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueForValidTwoPairs() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeAce = new Card(3, Suit.SPADE, Rank.ACE, false);
        Card heartNine = new Card(4, Suit.HEART, Rank.NINE, false);
        Card clubNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeAce, heartNine, clubNine);
        boolean result = pokerHandRecognizerService.isTwoPair(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);

        // Assert
        assertTrue(result);
        assertEquals(TWO_PAIRS, recognize);
    }

    @Test
    public void shouldReturnFalseForInValidTwoPairs() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeTwo, heartKing, cardNine);
        boolean result = pokerHandRecognizerService.isTwoPair(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueForValidOnePair() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartFour = new Card(4, Suit.HEART, Rank.FOUR, false);
        Card clubThree = new Card(5, Suit.CLUB, Rank.THREE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeTwo, heartFour, clubThree);
        boolean result = pokerHandRecognizerService.isOnePair(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);

        // Assert
        assertTrue(result);
        assertEquals(ONE_PAIR, recognize);
    }

    @Test
    public void shouldReturnFalseForInValidOnePair() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondKing, spadeTwo, heartKing, cardNine);
        boolean result = pokerHandRecognizerService.isOnePair(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueForHighCard() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondQueen = new Card(2, Suit.DIAMOND, Rank.QUEEN, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartSeven = new Card(4, Suit.HEART, Rank.SEVEN, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondQueen, spadeTwo, heartSeven, cardNine);
        boolean result = pokerHandRecognizerService.isHighCard(cards);
        HandType recognize = pokerHandRecognizerService.recognize(cards);
        // Assert
        assertTrue(result);
        assertEquals(HIGH_CARD, recognize);
    }

    @Test
    public void shouldReturnFalseForInvalidHighCard() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondQueen = new Card(2, Suit.DIAMOND, Rank.QUEEN, false);
        Card spadeQueen = new Card(3, Suit.SPADE, Rank.QUEEN, false);
        Card heartSeven = new Card(4, Suit.HEART, Rank.SEVEN, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondQueen, spadeQueen, heartSeven, cardNine);
        boolean result = pokerHandRecognizerService.isHighCard(cards);

        // Assert
        assertFalse(result);
    }

    @Test
    public void shouldRecognizeBestHand() {
        Card clubKing = new Card(1, Suit.SPADE, Rank.KING, false);
        Card diamondQueen = new Card(2, Suit.DIAMOND, Rank.QUEEN, false);
        Card spadeTwo = new Card(3, Suit.SPADE, Rank.TWO, false);
        Card heartSeven = new Card(4, Suit.HEART, Rank.SEVEN, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        List<Card> communityCards = ImmutableList.of(clubKing, diamondQueen, spadeTwo, heartSeven, cardNine);

        Card clubQueen = new Card(6, Suit.CLUB, Rank.QUEEN, false);
        Card diamondAce = new Card(7, Suit.DIAMOND, Rank.KING, false);
        List<Card> holeCards = ImmutableList.of(clubQueen, diamondAce);

        User user = new User("Yogesh", 1000D, false);

        PlayerPokerHand playerPokerHand = new PlayerPokerHand(user);
        playerPokerHand.setHoleCards(holeCards);

        HandType handType = pokerHandRecognizerService.recognize(communityCards, playerPokerHand);

        assertEquals(TWO_PAIRS, handType);
    }
}
