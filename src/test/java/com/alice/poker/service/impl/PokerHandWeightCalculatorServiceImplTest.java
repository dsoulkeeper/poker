package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.Rank;
import com.alice.poker.model.Suit;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class PokerHandWeightCalculatorServiceImplTest {

    @InjectMocks
    private PokerHandWeightCalculatorServiceImpl pokerHandWeightCalculatorService;

    @InjectMocks
    private PokerHandRecognizerServiceImpl pokerHandRecognizerService;

    @Test
    public void shouldCorrectlyCalculateWeightForHighCard() {
        // Arrange
        Card clubKing = new Card(1, Suit.CLUB, Rank.KING, false);
        Card diamondQueen = new Card(2, Suit.DIAMOND, Rank.QUEEN, false);
        Card spadeJack = new Card(3, Suit.SPADE, Rank.JACK, false);
        Card heartAce = new Card(4, Suit.HEART, Rank.ACE, false);
        Card cardNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubKing, diamondQueen, spadeJack, heartAce, cardNine);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateHighCardWeight(map);
        boolean highCard = pokerHandRecognizerService.isHighCard(cards);

        // assert
        assertTrue(highCard);
        assertEquals(59L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForOnePair() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card diamondAce = new Card(2, Suit.DIAMOND, Rank.ACE, false);
        Card spadeKing = new Card(3, Suit.SPADE, Rank.KING, false);
        Card heartQueen = new Card(4, Suit.HEART, Rank.QUEEN, false);
        Card cardJack = new Card(5, Suit.CLUB, Rank.JACK, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, diamondAce, spadeKing, heartQueen, cardJack);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateOnePairWeight(map);
        boolean onePair = pokerHandRecognizerService.isOnePair(cards);

        // assert
        assertTrue(onePair);
        assertEquals(876L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForTwoPair() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card diamondAce = new Card(2, Suit.DIAMOND, Rank.ACE, false);
        Card spadeKing = new Card(3, Suit.SPADE, Rank.KING, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardQueen = new Card(5, Suit.CLUB, Rank.QUEEN, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, diamondAce, spadeKing, heartKing, cardQueen);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateTwoPairWeight(map);
        boolean twoPair = pokerHandRecognizerService.isTwoPair(cards);

        // assert
        assertTrue(twoPair);
        assertEquals(24312L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForThreeOfAKind() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card diamondAce = new Card(2, Suit.DIAMOND, Rank.ACE, false);
        Card spadeAce = new Card(3, Suit.SPADE, Rank.ACE, false);
        Card heartKing = new Card(4, Suit.HEART, Rank.KING, false);
        Card cardQueen = new Card(5, Suit.CLUB, Rank.QUEEN, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, diamondAce, spadeAce, heartKing, cardQueen);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateThreeOfAKindWeight(map);
        boolean threeOfAKind = pokerHandRecognizerService.isThreeOfAKind(cards);

        // assert
        assertTrue(threeOfAKind);
        assertEquals(350025L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForStraight() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card diamondKing = new Card(2, Suit.DIAMOND, Rank.KING, false);
        Card spadeQueen = new Card(3, Suit.SPADE, Rank.QUEEN, false);
        Card heartJack = new Card(4, Suit.HEART, Rank.JACK, false);
        Card cardTen = new Card(5, Suit.CLUB, Rank.TEN, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, diamondKing, spadeQueen, heartJack, cardTen);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateStraightWeight(map);
        boolean straight = pokerHandRecognizerService.isStraight(cards);

        // assert
        assertTrue(straight);
        assertEquals(21_006_000L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForFlush() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card clubKing = new Card(2, Suit.CLUB, Rank.KING, false);
        Card clubQueen = new Card(3, Suit.CLUB, Rank.QUEEN, false);
        Card clubJack = new Card(4, Suit.CLUB, Rank.JACK, false);
        Card clubNine = new Card(5, Suit.CLUB, Rank.NINE, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, clubKing, clubQueen, clubJack, clubNine);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateFlushWeight(map);
        boolean flush = pokerHandRecognizerService.isFlush(cards);

        // assert
        assertTrue(flush);
        assertEquals(1_239_359_900L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForFullHouse() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card diamondAce = new Card(2, Suit.DIAMOND, Rank.ACE, false);
        Card heartAce = new Card(3, Suit.HEART, Rank.ACE, false);
        Card clubKing = new Card(4, Suit.CLUB, Rank.KING, false);
        Card spadeKing = new Card(5, Suit.SPADE, Rank.KING, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, diamondAce, heartAce, clubKing, spadeKing);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateFullHouseWeight(map);
        boolean fullHouse = pokerHandRecognizerService.isFullHouse(cards);

        // assert
        assertTrue(fullHouse);
        assertEquals(33_462_720_000L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForFourOfAKind() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card diamondAce = new Card(2, Suit.DIAMOND, Rank.ACE, false);
        Card heartAce = new Card(3, Suit.HEART, Rank.ACE, false);
        Card spadeAce = new Card(4, Suit.SPADE, Rank.ACE, false);
        Card spadeKing = new Card(5, Suit.SPADE, Rank.KING, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, diamondAce, heartAce, spadeAce, spadeKing);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateFourOfAKindWeight(map);
        boolean fourOfAKind = pokerHandRecognizerService.isFourOfAKind(cards);

        // assert
        assertTrue(fourOfAKind);
        assertEquals(468_478_081_413L, weight.longValue());
    }

    @Test
    public void shouldCorrectlyCalculateWeightForStraightFlush() {
        // Arrange
        Card clubAce = new Card(1, Suit.CLUB, Rank.ACE, false);
        Card clubKing = new Card(2, Suit.CLUB, Rank.KING, false);
        Card clubQueen = new Card(3, Suit.CLUB, Rank.QUEEN, false);
        Card clubJack = new Card(4, Suit.CLUB, Rank.JACK, false);
        Card clubTen = new Card(5, Suit.CLUB, Rank.TEN, false);

        // Act
        ImmutableList<Card> cards = ImmutableList.of(clubAce, clubKing, clubQueen, clubJack, clubTen);
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        Long weight = pokerHandWeightCalculatorService.calculateStraightFlushWeight(map);
        boolean straightFlush = pokerHandRecognizerService.isStraightFlush(cards);

        // assert
        assertTrue(straightFlush);
        assertEquals(28_108_684_884_780L, weight.longValue());
    }
}
