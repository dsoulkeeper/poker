package com.alice.poker.model;

import org.junit.Test;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static org.junit.Assert.assertEquals;

public class DeckTest {

    @Test
    public void deckShouldHave52Cards() {
        Deck deck = new Deck();
        assertEquals("Deck size should be 52", deck.getCards().size(), 52);
    }

    @Test
    public void eachSuitShouldHave13Cards() {
        Deck deck = new Deck();
        List<Card> cards = deck.getCards();
        Map<Suit, Long> count = cards.stream().collect(groupingBy(Card::getSuit, counting()));

        assertEquals("Diamond suit should have 13 cards", count.get(Suit.DIAMOND).intValue(), 13);
        assertEquals("Club suit should have 13 cards", count.get(Suit.CLUB).intValue(), 13);
        assertEquals("Spade suit should have 13 cards", count.get(Suit.SPADE).intValue(), 13);
        assertEquals("Heart suit should have 13 cards", count.get(Suit.HEART).intValue(), 13);
    }
}
