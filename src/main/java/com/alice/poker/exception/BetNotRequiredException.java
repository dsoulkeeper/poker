package com.alice.poker.exception;

public class BetNotRequiredException extends Exception {
    public BetNotRequiredException(final String message) {
        super(message);
    }
}
