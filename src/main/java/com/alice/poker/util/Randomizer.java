package com.alice.poker.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Randomizer {

    public int generate(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}
