package com.alice.poker.service;

import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;

public interface InformationService {
    void showHoleCardsToUsers(PokerHand pokerHand);

    void showFlopCardsToUsers(PokerHand pokerHand);

    void showTurnCardToUsers(PokerHand pokerHand);

    void showRiverCardToUsers(PokerHand pokerHand);

    void showWinningSummary(PokerHand pokerHand, PlayerPokerHand playerPokerHand);
}
