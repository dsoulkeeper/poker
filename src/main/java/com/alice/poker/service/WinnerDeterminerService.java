package com.alice.poker.service;

import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;

public interface WinnerDeterminerService {

    PlayerPokerHand determinerWinner(PokerHand pokerHand);
}
