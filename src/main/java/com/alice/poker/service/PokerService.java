package com.alice.poker.service;

import com.alice.poker.model.User;

import java.util.List;

public interface PokerService {

    void play(List<User> players);
}
