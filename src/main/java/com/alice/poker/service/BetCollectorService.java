package com.alice.poker.service;

import com.alice.poker.exception.BetNotRequiredException;
import com.alice.poker.model.Bet;
import com.alice.poker.model.CollectBetContext;

public interface BetCollectorService {
    Bet collectBet(CollectBetContext collectBetContext) throws BetNotRequiredException;
}
