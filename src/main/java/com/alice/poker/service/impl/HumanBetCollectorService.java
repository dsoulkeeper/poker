package com.alice.poker.service.impl;

import com.alice.poker.exception.BetNotRequiredException;
import com.alice.poker.model.Bet;
import com.alice.poker.model.CollectBetContext;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.model.Round;
import com.alice.poker.model.User;
import com.alice.poker.service.BettingService;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.alice.poker.model.BetType.CHECK;
import static com.alice.poker.model.BetType.FOLD;

@Slf4j
public class HumanBetCollectorService extends AbstractBetCollectorService {

    private static final int CHOICE_FOLD = 1;
    private static final int CHOICE_CHECK = 2;
    private static final int CHOICE_RAISE = 3;
    private static final int CHOICE_CALL = 4;

    @Override
    public Bet collectBet(final CollectBetContext collectBetContext) throws BetNotRequiredException {
        verifyPreconditions(collectBetContext);
        return askUserToBet(collectBetContext);
    }

    private Bet askUserToBet(CollectBetContext collectBetContext) throws BetNotRequiredException {
        PlayerPokerHand playerPokerHand = collectBetContext.getPlayerPokerHand();
        Round round = collectBetContext.getRound();
        User user = playerPokerHand.getUser();
        Integer choice = showAndReadUserChoice(playerPokerHand.getUser());
        switch (choice) {
            case CHOICE_FOLD:
                return handleFoldBet(collectBetContext);
            case CHOICE_CHECK:
                return handleCheckBet(collectBetContext);
            case CHOICE_RAISE:
                return handleRaiseBet(collectBetContext);
            case CHOICE_CALL:
                return handleCallBet(collectBetContext);
            default:
                log.info("Unknown choice. Please choose again.");
                return collectBet(collectBetContext);
        }
    }

    private Bet handleFoldBet(CollectBetContext collectBetContext) {
        log.info(collectBetContext.getPlayerPokerHand().getUser().getName() + " you have folded.");
        collectBetContext.getPlayerPokerHand().setFolded(true);
        return createBet(collectBetContext.getPlayerPokerHand(), collectBetContext.getRound(), FOLD);
    }

    private Bet handleCheckBet(CollectBetContext collectBetContext) throws BetNotRequiredException {
        String name = collectBetContext.getPlayerPokerHand().getUser().getName();
        if (!canCheck(collectBetContext.getLastPlayerBet())) {
            log.info(name + " you can't check at this point.");
            return collectBet(collectBetContext);
        } else {
            log.info(name + " you have checked.");
            return createBet(collectBetContext.getPlayerPokerHand(), collectBetContext.getRound(), CHECK);
        }
    }

    private Bet handleRaiseBet(CollectBetContext collectBetContext) throws BetNotRequiredException {
        Double raise = readUserBet(collectBetContext.getPlayerPokerHand().getUser());
        if (isValidBet(raise, collectBetContext.getPokerHand(), collectBetContext.getPlayerPokerHand())) {
            return createRaiseOrAllInBet(raise, collectBetContext.getPlayerPokerHand(), collectBetContext.getRound());
        } else {
            return collectBet(collectBetContext);
        }
    }

    private Bet handleCallBet(CollectBetContext collectBetContext) throws BetNotRequiredException {
        if (!canCall(collectBetContext.getLastPlayerBet())) {
            log.info(collectBetContext.getPlayerPokerHand().getUser().getName() + " you can't call at this point.");
            return collectBet(collectBetContext);
        } else {
            return createCallOrAllInBet(collectBetContext.getPlayerPokerHand(), collectBetContext.getRound(),
                    collectBetContext.getBettingService().getRequiredAmountToCall(collectBetContext.getPokerHand(),
                            collectBetContext.getPlayerPokerHand()));
        }
    }

    private void verifyPreconditions(CollectBetContext collectBetContext) throws BetNotRequiredException {
        PlayerPokerHand playerPokerHand = collectBetContext.getPlayerPokerHand();
        User user = playerPokerHand.getUser();

        if (playerPokerHand.isFolded()) {
            log.info(user.getName() + " skipping your turn as you have folded the hand");
            throw new BetNotRequiredException("Folded.");
        } else if (playerPokerHand.isAllIn()) {
            log.info(user.getName() + " is already all in.");
            throw new BetNotRequiredException("All in.");
        } else if (canSkipTurn(collectBetContext.getPokerHand(), playerPokerHand, collectBetContext.getRound(), collectBetContext.getBettingService())) {
            throw new BetNotRequiredException("Already acted.");
        }
    }

    private boolean canSkipTurn(PokerHand pokerHand, PlayerPokerHand playerPokerHand, Round round, BettingService bettingService) {
        Double requiredAmountToCall = bettingService.getRequiredAmountToCall(pokerHand, playerPokerHand);
        Double balance = playerPokerHand.getUser().getBalance();
        if (requiredAmountToCall == 0D && playerPokerHand.hasBetRound(round)) {
            // that means this player was the original raiser.
            return true;
        } else if (requiredAmountToCall < 0D) {
            // this means our raise has put all users to all in and this is extra amount returned to us.
            log.info(playerPokerHand.getUser().getName() + " No one matched your raise 100%. Returning to you: $" + Math.abs(requiredAmountToCall));
            playerPokerHand.getUser().setBalance(balance + Math.abs(requiredAmountToCall));
            return true;
        }
        return false;
    }

    private boolean canCheck(Bet lastPlayerBet) {
        if (lastPlayerBet == null || lastPlayerBet.getBetType().equals(CHECK)) {
            return true;
        } else if (lastPlayerBet.getBetType().equals(FOLD)) {
            return canCheck(lastPlayerBet.getPreviousBet());
        }
        return false;
    }

    private boolean canCall(Bet lastPlayerBet) {
        return !canCheck(lastPlayerBet);
    }

    private Integer showAndReadUserChoice(User user) {
        log.info(user.getName() + " please choose your action");
        log.info("1 to Fold");
        log.info("2 to Check");
        log.info("3 to Raise");
        log.info("4 to Call");

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            return Integer.parseInt(br.readLine());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } catch (NumberFormatException e) {
            log.info(user.getName() + " please enter correct choice");
        }
        return showAndReadUserChoice(user);
    }

    private Double readUserBet(User user) {
        log.info(user.getName() + " enter your bet");
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            return Double.parseDouble(br.readLine());
        } catch (NumberFormatException e) {
            log.info(user.getName() + " Please enter correct value");
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return readUserBet(user);
    }

    boolean isValidBet(Double amount, PokerHand pokerHand, PlayerPokerHand playerPokerHand) {
        if (amount < pokerHand.getBigBlind()) {
            log.info(playerPokerHand.getUser().getName() + " bet is less the big blind.");
            return false;
        } else {
            Double balance = playerPokerHand.getUser().getBalance();
            if ((balance - amount) < 0) {
                log.info(playerPokerHand.getUser().getName() + " incorrect bet. You only have $" + balance + ".");
                return false;
            }
        }
        return true;
    }
}
