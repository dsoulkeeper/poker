package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.HandType;
import com.alice.poker.model.Rank;
import com.alice.poker.service.PokerHandWeightCalculatorService;
import com.google.common.annotations.VisibleForTesting;

import java.util.List;
import java.util.Map;

import static com.alice.poker.model.HandType.FLUSH;
import static com.alice.poker.model.HandType.FOUR_OF_A_KIND;
import static com.alice.poker.model.HandType.FULL_HOUSE;
import static com.alice.poker.model.HandType.HIGH_CARD;
import static com.alice.poker.model.HandType.ONE_PAIR;
import static com.alice.poker.model.HandType.STRAIGHT;
import static com.alice.poker.model.HandType.STRAIGHT_FLUSH;
import static com.alice.poker.model.HandType.THREE_OF_A_KIND;
import static com.alice.poker.model.HandType.TWO_PAIRS;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class PokerHandWeightCalculatorServiceImpl implements PokerHandWeightCalculatorService {

    @Override
    public Long calculate(final HandType handType, final List<Card> cards) {
        checkArgument(cards.size() == 5, "Cards are not equal to 5. Can't weigh the poker hand.");
        checkArgument(handType != null, "Hand type not yet determined. Can't weigh the poker hand.");

        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        switch (handType) {
            case HIGH_CARD:
                return calculateHighCardWeight(map);
            case ONE_PAIR:
                return calculateOnePairWeight(map);
            case TWO_PAIRS:
                return calculateTwoPairWeight(map);
            case THREE_OF_A_KIND:
                return calculateThreeOfAKindWeight(map);
            case STRAIGHT:
                return calculateStraightWeight(map);
            case FLUSH:
                return calculateFlushWeight(map);
            case FULL_HOUSE:
                return calculateFullHouseWeight(map);
            case FOUR_OF_A_KIND:
                return calculateFourOfAKindWeight(map);
            case STRAIGHT_FLUSH:
                return calculateStraightFlushWeight(map);
            default:
                throw new IllegalStateException("Unsupported hand type found - " + handType);
        }
    }

    @VisibleForTesting
    Long calculateHighCardWeight(Map<Rank, Long> map) {
        return calculateWeight(HIGH_CARD, map);
    }

    @VisibleForTesting
    Long calculateOnePairWeight(Map<Rank, Long> map) {
        return calculateWeight(ONE_PAIR, map, 1, 2);
    }

    @VisibleForTesting
    Long calculateTwoPairWeight(final Map<Rank, Long> map) {
        return calculateWeight(TWO_PAIRS, map, 1, 2);
    }

    @VisibleForTesting
    Long calculateThreeOfAKindWeight(final Map<Rank, Long> map) {
        return calculateWeight(THREE_OF_A_KIND, map, 1, 3);
    }

    @VisibleForTesting
    Long calculateStraightWeight(final Map<Rank, Long> map) {
        return calculateWeight(STRAIGHT, map);
    }

    @VisibleForTesting
    Long calculateFlushWeight(final Map<Rank, Long> map) {
        return calculateWeight(FLUSH, map);
    }

    @VisibleForTesting
    Long calculateFullHouseWeight(final Map<Rank, Long> map) {
        return calculateWeight(FULL_HOUSE, map);
    }

    @VisibleForTesting
    Long calculateFourOfAKindWeight(final Map<Rank, Long> map) {
        return calculateWeight(FOUR_OF_A_KIND, map, 1, 4);
    }

    @VisibleForTesting
    Long calculateStraightFlushWeight(final Map<Rank, Long> map) {
        return calculateWeight(STRAIGHT_FLUSH, map);
    }

    private Long calculateWeight(final HandType handType, final Map<Rank, Long> map) {
        return handType.getWeight() * (map.keySet().stream()
                .map(Rank::getWeight)
                .mapToLong(Integer::intValue)
                .sum());
    }

    private Long calculateWeight(final HandType handType, final Map<Rank, Long> map, int adderCount, int multiplierCount) {
        Long weight = 0L;

        for (Map.Entry<Rank, Long> entry : map.entrySet()) {
            if (entry.getValue() == adderCount) {
                weight += entry.getKey().getWeight();
            } else if (entry.getValue() == multiplierCount) {
                weight += handType.getWeight() * entry.getKey().getWeight();
            }
        }
        return weight;
    }
}
