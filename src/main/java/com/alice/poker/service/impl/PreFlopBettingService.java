package com.alice.poker.service.impl;

import com.alice.poker.model.Bet;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.Round;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class PreFlopBettingService extends AbstractBettingService {

    @Override
    protected Round getCurrentRound() {
        return Round.PREFLOP;
    }

    @Override
    protected List<Bet> getCurrentRoundBets(PlayerPokerHand playerPokerHand) {
        return playerPokerHand.getPreFlopBets();
    }
}
