package com.alice.poker.service.impl;

import com.alice.poker.model.Bet;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.Round;

import java.util.List;

public class RiverBettingService extends AbstractBettingService {

    @Override
    protected Round getCurrentRound() {
        return Round.RIVER;
    }

    @Override
    protected List<Bet> getCurrentRoundBets(final PlayerPokerHand playerPokerHand) {
        return playerPokerHand.getRiverBets();
    }
}
