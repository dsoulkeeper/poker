package com.alice.poker.service.impl;

import com.alice.poker.exception.BetNotRequiredException;
import com.alice.poker.model.Bet;
import com.alice.poker.model.CollectBetContext;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.model.Round;
import com.alice.poker.service.BetCollectorService;
import com.alice.poker.service.BettingService;

import java.util.List;

public abstract class AbstractBettingService implements BettingService {

    private BetCollectorService botBetCollectorService = new BotBetCollectorService();
    private BetCollectorService humanBetCollectorService = new HumanBetCollectorService();

    @Override
    public void collectBets(final PokerHand pokerHand) {
        List<PlayerPokerHand> playerPokerHands = pokerHand.getPlayerPokerHands();
        Bet lastPlayerBet = pokerHand.getLastBet();
        for (PlayerPokerHand playerPokerHand : playerPokerHands) {
            try {
                Bet bet;
                CollectBetContext collectBetContext = new CollectBetContext(playerPokerHand, pokerHand, lastPlayerBet,
                        getCurrentRound(), this);
                if (playerPokerHand.getUser().isBot()) {
                    bet = botBetCollectorService.collectBet(collectBetContext);
                } else {
                    bet = humanBetCollectorService.collectBet(collectBetContext);
                }
                bet.setPreviousBet(lastPlayerBet);
                lastPlayerBet = bet;
                getCurrentRoundBets(playerPokerHand).add(bet);
                pokerHand.setLastBet(lastPlayerBet);
            } catch (BetNotRequiredException e) {
                // ignore
            }
        }
    }

    @Override
    public Double getMaxTotalBetForRound(PokerHand pokerHand) {
        List<PlayerPokerHand> playerPokerHands = pokerHand.getPlayerPokerHands();
        Double maxBet = 0D;
        for (PlayerPokerHand playerPokerHand : playerPokerHands) {
            Double allMyBetsForRound = getMyTotalBetAmountForRound(playerPokerHand);
            if (maxBet < allMyBetsForRound) {
                maxBet = allMyBetsForRound;
            }
        }
        return maxBet;
    }

    @Override
    public Double getRequiredAmountToCall(PokerHand pokerHand, PlayerPokerHand playerPokerHand) {
        Double allMyBetsForRound = getMyTotalBetAmountForRound(playerPokerHand);
        Double collectiveMaxBetForRound = getMaxTotalBetForRound(pokerHand);
        return collectiveMaxBetForRound - allMyBetsForRound;
    }

    @Override
    public boolean isBettingRoundFinished(PokerHand pokerHand) {
        Double collectiveMaxBetForRound = getMaxTotalBetForRound(pokerHand);
        List<PlayerPokerHand> playerPokerHands = pokerHand.getPlayerPokerHands();
        for (PlayerPokerHand playerPokerHand : playerPokerHands) {
            if (playerPokerHand.isFolded()) {
                continue;
            }
            Double allMyBetsForRound = getMyTotalBetAmountForRound(playerPokerHand);
            if (!allMyBetsForRound.equals(collectiveMaxBetForRound)) {
                if (allMyBetsForRound < collectiveMaxBetForRound && playerPokerHand.isAllIn()) {
                    continue;
                }
                return false;
            }
        }
        return true;
    }

    @Override
    public Double getMyTotalBetAmountForRound(PlayerPokerHand playerPokerHand) {
        return getCurrentRoundBets(playerPokerHand).stream()
                .filter(bet -> bet.getAmount() != null)
                .mapToDouble(Bet::getAmount)
                .sum();
    }

    protected abstract Round getCurrentRound();

    protected abstract List<Bet> getCurrentRoundBets(PlayerPokerHand playerPokerHand);

}
