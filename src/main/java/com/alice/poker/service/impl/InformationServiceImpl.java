package com.alice.poker.service.impl;

import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.model.User;
import com.alice.poker.service.InformationService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class InformationServiceImpl implements InformationService {

    private static final String SEPARATOR = "=============GAME SUMMARY================";
    private static final String SMALL_SEPARATOR = "-----";

    @Override
    public void showHoleCardsToUsers(final PokerHand pokerHand) {
        List<PlayerPokerHand> playerPokerHands = pokerHand.getPlayerPokerHands();
        for (PlayerPokerHand playerPokerHand : playerPokerHands) {
            User user = playerPokerHand.getUser();
            if (!user.isBot()) {
                log.info(user.getName() + "'s hole cards:" + playerPokerHand.getHoleCards());
            }
        }
    }

    @Override
    public void showFlopCardsToUsers(final PokerHand pokerHand) {
        log.info(SMALL_SEPARATOR);
        log.info("Reminding you your hole cards");
        showHoleCardsToUsers(pokerHand);
        log.info(SMALL_SEPARATOR);
        log.info("Flop Cards:" + pokerHand.getFlopCards());
    }

    @Override
    public void showTurnCardToUsers(final PokerHand pokerHand) {
        log.info(SMALL_SEPARATOR);
        log.info("Reminding you your hole cards");
        showHoleCardsToUsers(pokerHand);
        log.info(SMALL_SEPARATOR);
        log.info("Turn Card:" + pokerHand.getTurnCard());
    }

    @Override
    public void showRiverCardToUsers(final PokerHand pokerHand) {
        log.info(SMALL_SEPARATOR);
        log.info("Reminding you your hole cards");
        log.info(SMALL_SEPARATOR);
        showHoleCardsToUsers(pokerHand);
        log.info("River Card:" + pokerHand.getRiverCard());
    }

    @Override
    public void showWinningSummary(final PokerHand pokerHand, final PlayerPokerHand playerPokerHand) {
        log.info(SEPARATOR);
        List<PlayerPokerHand> playerPokerHands = pokerHand.getPlayerPokerHands();
        for (PlayerPokerHand hand : playerPokerHands) {
            log.info(hand.getUser().getName() + "'s hole cards: " + hand.getHoleCards());
        }
        log.info("Flop Cards: " + pokerHand.getFlopCards());
        log.info("Turn Card: " + pokerHand.getTurnCard());
        log.info("River Cards: " + pokerHand.getRiverCard());

        log.info(SMALL_SEPARATOR);
        for (PlayerPokerHand hand : playerPokerHands) {
            log.info(hand.getUser().getName() + "'s best combination - " + hand.getHandType() + " - " + hand.getBestCombination());
        }
        log.info(SMALL_SEPARATOR);
        log.info("Winner is " + playerPokerHand.getUser().getName());
        log.info("Winning Hand - " + playerPokerHand.getHandType() + " - " + playerPokerHand.getBestCombination());
        log.info("Pot size: " + pokerHand.calculateWinningAmount());
        log.info(SMALL_SEPARATOR);
        log.info("Balances after this game");
        for (PlayerPokerHand hand : playerPokerHands) {
            log.info(hand.getUser().getName() + ": " + hand.getUser().getBalance());
        }
    }
}
