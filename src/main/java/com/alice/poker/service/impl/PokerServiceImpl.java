package com.alice.poker.service.impl;

import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.model.User;
import com.alice.poker.service.BettingService;
import com.alice.poker.service.DealerService;
import com.alice.poker.service.InformationService;
import com.alice.poker.service.PokerHandRecognizerService;
import com.alice.poker.service.PokerHandWeightCalculatorService;
import com.alice.poker.service.PokerService;
import com.alice.poker.service.WinnerDeterminerService;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
public class PokerServiceImpl implements PokerService {

    private final BettingService preFlopBettingService = new PreFlopBettingService();
    private final BettingService flopBettingService = new FlopBettingService();
    private final BettingService turnBettingService = new TurnBettingService();
    private final BettingService riverBettingService = new RiverBettingService();
    private final DealerService dealerService = new DealerServiceImpl();
    private final InformationService informationService = new InformationServiceImpl();
    private final PokerHandWeightCalculatorService pokerHandWeightCalculatorService = new PokerHandWeightCalculatorServiceImpl();
    private final PokerHandRecognizerService pokerHandRecognizerService = new PokerHandRecognizerServiceImpl(pokerHandWeightCalculatorService);
    private final WinnerDeterminerService winnerDeterminerService = new WinnerDeterminerServiceImpl(pokerHandRecognizerService);

    @Override
    public void play(final List<User> players) {
        PokerHand pokerHand = buildPokerHand(sanitize(players));

        // Preflop card dealing and betting
        dealerService.dealHoleCards(pokerHand);
        informationService.showHoleCardsToUsers(pokerHand);
        finishCurrentRound(pokerHand, preFlopBettingService);

        // Flop card dealing and betting
        log.info("PreFlop round finished. Starting flop round.");
        dealerService.dealFlop(pokerHand);
        informationService.showFlopCardsToUsers(pokerHand);
        finishCurrentRound(pokerHand, flopBettingService);

        // Turn card dealing and betting
        log.info("Flop round finished. Starting turn round.");
        dealerService.dealTurn(pokerHand);
        informationService.showTurnCardToUsers(pokerHand);
        finishCurrentRound(pokerHand, turnBettingService);

        // River card dealing and betting
        log.info("Turn round finished. Starting river round.");
        dealerService.dealRiver(pokerHand);
        informationService.showRiverCardToUsers(pokerHand);
        finishCurrentRound(pokerHand, riverBettingService);

        log.info("Game finished.");
        declareWinner(pokerHand);
    }

    private void declareWinner(final PokerHand pokerHand) {
        PlayerPokerHand playerPokerHand = winnerDeterminerService.determinerWinner(pokerHand);
        Double winningAmount = pokerHand.calculateWinningAmount();
        Double balance = playerPokerHand.getUser().getBalance();
        playerPokerHand.getUser().setBalance(balance + winningAmount);
        informationService.showWinningSummary(pokerHand, playerPokerHand);
    }

    private void finishCurrentRound(PokerHand pokerHand, BettingService bettingService) {
        bettingService.collectBets(pokerHand);
        if (!bettingService.isBettingRoundFinished(pokerHand)) {
            finishCurrentRound(pokerHand, bettingService);
        }
        // round finished hence last best doesn't matter for new round.
        pokerHand.setLastBet(null);
    }

    private PokerHand buildPokerHand(List<User> players) {
        PokerHand pokerHand = new PokerHand();
        List<PlayerPokerHand> playerPokerHands = players.stream().map(player -> new PlayerPokerHand(player))
                .collect(toList());
        pokerHand.setPlayerPokerHands(playerPokerHands);
        return pokerHand;
    }

    private List<User> sanitize(final List<User> players) {
        List<User> sanitizedList = new ArrayList<>();
        for (User user : players) {
            if (user.getBalance() <= 0D) {
                if (!user.isBot()) {
                    log.info(user.getName() + " you do not have sufficient balance. Please restart the JVM or you will be dropped.");
                } else {
                    log.info(user.getName() + " will be dropped in next game due to insufficient balance.");
                }
            } else {
                sanitizedList.add(user);
            }
        }
        if (sanitizedList.size() == 1) {
            log.info(sanitizedList.get(0).getName() + " you have beaten all players! Congrats!! Please restart the JVM to start new game.");
            System.exit(0);
        }
        return sanitizedList;
    }

}
