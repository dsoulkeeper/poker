package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.Deck;
import com.alice.poker.model.PokerHand;
import com.alice.poker.service.DealerService;
import com.alice.poker.util.Randomizer;

import java.util.ArrayList;
import java.util.List;

public class DealerServiceImpl implements DealerService {

    @Override
    public void dealHoleCards(final PokerHand pokerHand) {
        Deck deck = pokerHand.getDeck();
        pokerHand.getPlayerPokerHands()
                .forEach(playerPokerHand -> playerPokerHand.setHoleCards(dealRandomly(deck, 2)));
    }

    @Override
    public void dealFlop(final PokerHand pokerHand) {
        List<Card> cards = dealRandomly(pokerHand.getDeck(), 3);
        pokerHand.setFlopCards(cards);
    }

    @Override
    public void dealTurn(final PokerHand pokerHand) {
        Card card = dealRandomly(pokerHand.getDeck(), 1).get(0);
        pokerHand.setTurnCard(card);
    }

    @Override
    public void dealRiver(final PokerHand pokerHand) {
        Card card = dealRandomly(pokerHand.getDeck(), 1).get(0);
        pokerHand.setRiverCard(card);
    }

    private List<Card> dealRandomly(Deck deck, int numOfCards) {
        int dealtCardsCount = 0;
        List<Card> dealtCards = new ArrayList<>();

        while (dealtCardsCount != numOfCards) {
            int cardIndex = Randomizer.generate(0, 51);
            Card card = deck.getCard(cardIndex);
            if (!card.isUsed()) {
                dealtCardsCount++;
                card.setUsed(true);
                dealtCards.add(card);
            }
        }
        return dealtCards;
    }
}
