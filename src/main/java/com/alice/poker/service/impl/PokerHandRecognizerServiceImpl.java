package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.HandType;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.Rank;
import com.alice.poker.model.Suit;
import com.alice.poker.service.PokerHandRecognizerService;
import com.alice.poker.service.PokerHandWeightCalculatorService;
import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.alice.poker.model.HandType.FLUSH;
import static com.alice.poker.model.HandType.FOUR_OF_A_KIND;
import static com.alice.poker.model.HandType.FULL_HOUSE;
import static com.alice.poker.model.HandType.HIGH_CARD;
import static com.alice.poker.model.HandType.ONE_PAIR;
import static com.alice.poker.model.HandType.STRAIGHT;
import static com.alice.poker.model.HandType.STRAIGHT_FLUSH;
import static com.alice.poker.model.HandType.THREE_OF_A_KIND;
import static com.alice.poker.model.HandType.TWO_PAIRS;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@AllArgsConstructor
public class PokerHandRecognizerServiceImpl implements PokerHandRecognizerService {

    // This is a cached index array that contains all the index combination of choosing 5 cards out of 7.
    // This list is used when system will have to find best poker hand for use from 5 community cards + 2 hole cards
    private static List<List<Integer>> handCombinations = new ArrayList<>();

    private final PokerHandWeightCalculatorService pokerHandWeightCalculatorService;

    static {
        getAllCombinationsRecursively(new int[]{0, 1, 2, 3, 4, 5, 6}, new int[5], 0, 6, 0, 5, handCombinations);
    }

    private static void getAllCombinationsRecursively(int totalCards[], int currentCombo[], int start,
                                                      int end, int index, int windowSize, List<List<Integer>> collector) {
        if (index == windowSize) {
            List<Integer> combination = new ArrayList<>();
            for (int j = 0; j < windowSize; j++) {
                combination.add(currentCombo[j]);
            }
            collector.add(combination);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= windowSize - index; i++) {
            currentCombo[index] = totalCards[i];
            getAllCombinationsRecursively(totalCards, currentCombo, i + 1, end, index + 1, windowSize, collector);
        }
    }

    @Override
    public HandType recognize(List<Card> communityCards, PlayerPokerHand playerPokerHand) {
        List<Card> cards = new ArrayList<>(communityCards);
        cards.addAll(playerPokerHand.getHoleCards());

        List<List<Card>> allHandsCombinations = getAllHandsCombinations(cards);

        List<Card> bestList = null;
        HandType bestHand = null;
        Long bestWeight = -1L;

        for (List<Card> combination : allHandsCombinations) {
            HandType handType = recognize(combination);
            Long calculatedWeight = pokerHandWeightCalculatorService.calculate(handType, combination);
            if (bestWeight < calculatedWeight) {
                bestWeight = calculatedWeight;
                bestHand = handType;
                bestList = combination;
            }
        }

        playerPokerHand.setBestCombination(bestList);
        playerPokerHand.setHandType(bestHand);
        playerPokerHand.setWeight(bestWeight);
        return bestHand;
    }

    private List<List<Card>> getAllHandsCombinations(List<Card> cards) {
        return handCombinations.stream()
                .map(list -> list.stream()
                        .map(cards::get)
                        .collect(toList()))
                .collect(toList());
    }

    @VisibleForTesting
    HandType recognize(List<Card> cards) {
        checkArgument(cards.size() == 5, "Cards are not equal to 5. Can't recognize poker hand.");
        if (isStraightFlush(cards)) {
            return STRAIGHT_FLUSH;
        } else if (isFourOfAKind(cards)) {
            return FOUR_OF_A_KIND;
        } else if (isFullHouse(cards)) {
            return FULL_HOUSE;
        } else if (isFlush(cards)) {
            return FLUSH;
        } else if (isStraight(cards)) {
            return STRAIGHT;
        } else if (isThreeOfAKind(cards)) {
            return THREE_OF_A_KIND;
        } else if (isTwoPair(cards)) {
            return TWO_PAIRS;
        } else if (isOnePair(cards)) {
            return ONE_PAIR;
        } else if (isHighCard(cards)) {
            return HIGH_CARD;
        }
        throw new IllegalStateException("Unable to recognize the hand: " + cards);
    }

    @VisibleForTesting
    boolean isStraightFlush(List<Card> cards) {
        return isFlush(cards) && isStraight(cards);
    }

    @VisibleForTesting
    boolean isFourOfAKind(List<Card> cards) {
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        if (map.size() != 2) {
            return false;
        }

        for (Map.Entry<Rank, Long> entry : map.entrySet()) {
            if (entry.getValue() == 4) {
                return true;
            }
        }

        return false;
    }

    @VisibleForTesting
    boolean isFullHouse(List<Card> cards) {
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        if (map.size() != 2) {
            return false;
        }

        boolean foundTwo = false;
        boolean foundThree = false;

        for (Map.Entry<Rank, Long> entry : map.entrySet()) {
            if (entry.getValue() == 2) {
                foundTwo = true;
            } else if (entry.getValue() == 3) {
                foundThree = true;
            }
        }

        return foundThree && foundTwo;
    }

    @VisibleForTesting
    boolean isFlush(List<Card> cards) {
        Map<Suit, Long> map = cards.stream().collect(groupingBy(Card::getSuit, counting()));
        return map.size() == 1;
    }

    @VisibleForTesting
    boolean isStraight(List<Card> cards) {
        List<Card> cardsCopy = new ArrayList<>(cards);
        Collections.sort(cardsCopy, comparing(card -> card.getRank().getWeight()));

        // handle special case of ACE,2,3,4,5
        if (cardsCopy.get(4).getRank() == Rank.ACE && areAllIncrementalByOne(cardsCopy, 4)
                && cardsCopy.get(0).getRank() == Rank.TWO) {
            return true;
        }

        // otherwise regular sequence check
        return areAllIncrementalByOne(cardsCopy, 5);
    }

    @VisibleForTesting
    boolean isThreeOfAKind(List<Card> cards) {
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));

        if (map.size() != 3) {
            return false;
        }

        boolean foundFirstOne = false;
        boolean foundSecondOne = false;
        boolean foundThree = false;

        for (Map.Entry<Rank, Long> entry : map.entrySet()) {
            if (entry.getValue() == 3) {
                foundThree = true;
            } else if (entry.getValue() == 1) {
                if (!foundFirstOne) {
                    foundFirstOne = true;
                } else {
                    foundSecondOne = true;
                }
            }
        }
        return foundFirstOne && foundSecondOne && foundThree;
    }

    @VisibleForTesting
    boolean isTwoPair(List<Card> cards) {
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));

        if (map.size() != 3) {
            return false;
        }

        boolean foundPairOne = false;
        boolean foundPairTwo = false;
        boolean foundKicker = false;

        for (Map.Entry<Rank, Long> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                foundKicker = true;
            } else if (entry.getValue() == 2) {
                if (!foundPairOne) {
                    foundPairOne = true;
                } else {
                    foundPairTwo = true;
                }
            }
        }

        return foundPairOne && foundPairTwo && foundKicker;
    }

    @VisibleForTesting
    boolean isOnePair(List<Card> cards) {
        // no further checking is required as 4 size is only possible when there is exactly one pair
        return groupByAndMatchSize(cards, 4);
    }

    @VisibleForTesting
    boolean isHighCard(List<Card> cards) {
        // no further checking is required as 5 size is only possible when there are no type of hand at all
        return groupByAndMatchSize(cards, 5);
    }

    private boolean groupByAndMatchSize(List<Card> cards, int expectedSize) {
        Map<Rank, Long> map = cards.stream().collect(groupingBy(Card::getRank, counting()));
        return map.size() == expectedSize;
    }

    // This method checks wheather all 5 cards are incremental by one
    private boolean areAllIncrementalByOne(List<Card> cards, int till) {
        int count = cards.get(0).getRank().getWeight() + 1;
        for (int i = 1; i < till && i < cards.size(); i++) {
            if (count != cards.get(i).getRank().getWeight()) {
                return false;
            }
            count++;
        }
        return true;
    }

}
