package com.alice.poker.service.impl;

import com.alice.poker.exception.BetNotRequiredException;
import com.alice.poker.model.Bet;
import com.alice.poker.model.BetType;
import com.alice.poker.model.CollectBetContext;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.model.Round;
import com.alice.poker.model.User;
import com.alice.poker.service.BettingService;
import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

import static com.alice.poker.model.BetType.ALL_IN;
import static com.alice.poker.model.BetType.CALL;
import static com.alice.poker.model.BetType.CHECK;
import static com.alice.poker.model.BetType.FOLD;
import static com.alice.poker.model.BetType.RAISE;

@Slf4j
public class BotBetCollectorService extends AbstractBetCollectorService {

    private static final Set<BetType> CALL_OR_ALLIN_BET_TYPES = ImmutableSet.of(CALL, RAISE, ALL_IN);

    @Override
    public Bet collectBet(CollectBetContext collectBetContext) throws BetNotRequiredException {
        PlayerPokerHand playerPokerHand = collectBetContext.getPlayerPokerHand();

        User user = playerPokerHand.getUser();
        if (playerPokerHand.isAllIn()) {
            log.info(user.getName() + " is already all in.");
            throw new BetNotRequiredException("All in.");
        } else {
            return collectBetInternal(collectBetContext);
        }
    }

    private Bet collectBetInternal(CollectBetContext collectBetContext) throws BetNotRequiredException {
        Bet lastPlayerBet = collectBetContext.getLastPlayerBet();
        Round round = collectBetContext.getRound();
        PokerHand pokerHand = collectBetContext.getPokerHand();
        BettingService bettingService = collectBetContext.getBettingService();
        PlayerPokerHand playerPokerHand = collectBetContext.getPlayerPokerHand();

        if (lastPlayerBet == null || lastPlayerBet.getBetType().equals(CHECK)) {
            return doCheckBet(playerPokerHand, round);

        } else if (lastPlayerBet.getBetType().equals(FOLD)) {
            CollectBetContext newCollectionBetContext = new CollectBetContext(playerPokerHand, pokerHand,
                    lastPlayerBet.getPreviousBet(), round, bettingService);
            return collectBet(newCollectionBetContext);

        } else if (CALL_OR_ALLIN_BET_TYPES.contains(lastPlayerBet.getBetType())) {
            return createCallOrAllInBet(playerPokerHand, round, bettingService.getRequiredAmountToCall(pokerHand, playerPokerHand));
        }
        throw new IllegalStateException("Unexpected flow branch in game.");
    }

    private Bet doCheckBet(PlayerPokerHand playerPokerHand, Round round) {
        User user = playerPokerHand.getUser();
        Double balance = user.getBalance();
        // randomly choosing check or raise
        if (System.currentTimeMillis() % 2 == 0) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (balance > 0D && (balance / 2) > 0) {
                Bet bet = createRaiseOrAllInBet((balance / 2), playerPokerHand, round);
                log.info(user.getName() + " has " + bet.getBetType() + " for $" + bet.getAmount() + ". Balance left: $" + user.getBalance());
                return bet;
            }
        }
        log.info(user.getName() + " has checked.");
        return createBet(playerPokerHand, round, CHECK);
    }
}
