package com.alice.poker.service.impl;

import com.alice.poker.exception.BetNotRequiredException;
import com.alice.poker.model.Bet;
import com.alice.poker.model.BetType;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.Round;
import com.alice.poker.service.BetCollectorService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractBetCollectorService implements BetCollectorService {

    Bet createBet(PlayerPokerHand playerPokerHand, Round round, BetType betType) {
        Bet bet = new Bet();
        bet.setBetType(betType);
        bet.setRound(round);
        bet.setPlayerPokerHand(playerPokerHand);
        return bet;
    }

    Bet createCallOrAllInBet(PlayerPokerHand playerPokerHand, Round round, Double requiredAmountToCall) throws BetNotRequiredException {
        if (requiredAmountToCall == 0D) {
            // that means this player was the original raiser.
            throw new BetNotRequiredException("No bet required.");
        } else if (requiredAmountToCall < 0D) {
            // this means our raise has put all users to all in and this is extra amount returned to us.
            playerPokerHand.getUser().setBalance(playerPokerHand.getUser().getBalance() + Math.abs(requiredAmountToCall));
            throw new BetNotRequiredException("No bet required.");
        }
        return buildCallOrAllInBet(requiredAmountToCall, playerPokerHand, round);
    }

    private Bet buildCallOrAllInBet(Double requiredAmountToCall, PlayerPokerHand playerPokerHand, Round round) {
        Double currentBalance = playerPokerHand.getUser().getBalance();
        Double betAmount;
        Bet bet = new Bet();
        if (requiredAmountToCall >= currentBalance) {
            bet.setBetType(BetType.ALL_IN);
            betAmount = currentBalance;
            playerPokerHand.getUser().setBalance(0D);
            playerPokerHand.setAllIn(true);
        } else {
            bet.setBetType(BetType.CALL);
            betAmount = requiredAmountToCall;
            playerPokerHand.getUser().setBalance(currentBalance - requiredAmountToCall);
        }
        bet.setPlayerPokerHand(playerPokerHand);
        bet.setRound(round);
        bet.setAmount(betAmount);
        log.info(playerPokerHand.getUser().getName() + " you have " + bet.getBetType() + " for $" + betAmount +
                ". Balance left: $" + playerPokerHand.getUser().getBalance());
        return bet;
    }

    Bet createRaiseOrAllInBet(Double raise, PlayerPokerHand playerPokerHand, Round round) {
        Double balance = playerPokerHand.getUser().getBalance();
        BetType betType;
        if (balance - raise == 0D) {
            betType = BetType.ALL_IN;
            playerPokerHand.setAllIn(true);
        } else {
            betType = BetType.RAISE;
        }
        Bet bet = createBet(playerPokerHand, round, betType);
        bet.setAmount(raise);
        playerPokerHand.getUser().setBalance(balance - raise);
        return bet;
    }
}
