package com.alice.poker.service.impl;

import com.alice.poker.model.Card;
import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;
import com.alice.poker.service.PokerHandRecognizerService;
import com.alice.poker.service.WinnerDeterminerService;
import lombok.AllArgsConstructor;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;

@AllArgsConstructor
public class WinnerDeterminerServiceImpl implements WinnerDeterminerService {

    private final PokerHandRecognizerService pokerHandRecognizerService;

    @Override
    public PlayerPokerHand determinerWinner(final PokerHand pokerHand) {
        checkArgument(pokerHand.getPlayerPokerHands().size() >= 2, "Can't determine the winner. Insufficient players.");

        List<Card> communityCards = pokerHand.getCommunityCards();
        List<PlayerPokerHand> playerPokerHands = pokerHand.getPlayerPokerHands();

        PlayerPokerHand currentHandWinner = determinerWinner(communityCards, playerPokerHands.get(0), playerPokerHands.get(1));

        for (int i = 2; i < playerPokerHands.size(); i++) {
            currentHandWinner = determinerWinner(communityCards, currentHandWinner, playerPokerHands.get(i));
        }

        return currentHandWinner;
    }

    private PlayerPokerHand determinerWinner(List<Card> communityCards, PlayerPokerHand hand1, PlayerPokerHand hand2) {

        pokerHandRecognizerService.recognize(communityCards, hand1);
        pokerHandRecognizerService.recognize(communityCards, hand2);

        if (hand1.isFolded()) {
            return hand2;
        } else if (hand2.isFolded()) {
            return hand1;
        }

        return hand1.getWeight() > hand2.getWeight() ? hand1 : hand2;
    }
}
