package com.alice.poker.service;

import com.alice.poker.model.PokerHand;

public interface DealerService {

    void dealHoleCards(PokerHand pokerHand);

    void dealFlop(PokerHand pokerHand);

    void dealTurn(PokerHand pokerHand);

    void dealRiver(PokerHand pokerHand);
}
