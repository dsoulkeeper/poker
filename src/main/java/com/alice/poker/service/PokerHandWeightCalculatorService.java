package com.alice.poker.service;

import com.alice.poker.model.Card;
import com.alice.poker.model.HandType;

import java.util.List;

public interface PokerHandWeightCalculatorService {

    Long calculate(HandType handType, List<Card> list);
}
