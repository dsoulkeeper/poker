package com.alice.poker.service;

import com.alice.poker.model.Card;
import com.alice.poker.model.HandType;
import com.alice.poker.model.PlayerPokerHand;

import java.util.List;

public interface PokerHandRecognizerService {

    HandType recognize(List<Card> communityCards, PlayerPokerHand playerPokerHand);
}
