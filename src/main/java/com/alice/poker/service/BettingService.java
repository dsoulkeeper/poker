package com.alice.poker.service;

import com.alice.poker.model.PlayerPokerHand;
import com.alice.poker.model.PokerHand;

public interface BettingService {

    void collectBets(PokerHand pokerHand);

    boolean isBettingRoundFinished(PokerHand pokerHand);

    Double getRequiredAmountToCall(PokerHand pokerHand, PlayerPokerHand playerPokerHand);

    Double getMaxTotalBetForRound(PokerHand pokerHand);

    Double getMyTotalBetAmountForRound(PlayerPokerHand playerPokerHand);
}
