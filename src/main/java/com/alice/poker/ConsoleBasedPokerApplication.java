package com.alice.poker;

import com.alice.poker.model.User;
import com.alice.poker.service.PokerService;
import com.alice.poker.service.impl.PokerServiceImpl;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ConsoleBasedPokerApplication {

    private static final Double INITIAL_BALANCE = 1000D;

    private final PokerService pokerService = new PokerServiceImpl();

    public void start() throws Exception {
        printInfo();
        List<User> humanPlayers = getHumanPlayers();
        List<User> botPlayers = getBotPlayers(humanPlayers.size());
        List<User> allPlayers = new ArrayList<>(botPlayers);
        allPlayers.addAll(humanPlayers);
        if (allPlayers.size() <= 1) {
            log.info("Insufficient players. Need at least 2");
            start();
        }
        startNewGame(allPlayers);
    }

    private void printInfo() {
        log.info("Welcome to Poker Game.");
        log.info("1. You can play against multiple HUMAN players as well as multiple BOTS");
        log.info("2. Max players are limited to 9.");
        log.info("3. Starting balance for every player is $1000");
        log.info("4. Split pot is not implemented yet.");
        log.info("5. See how game progresses with 0 HUMAN PLAYERS AND 9 BOTS ;) ");
    }

    public void startNewGame(List<User> players) throws Exception {
        pokerService.play(players);
        promptUserToReplayGame(players);
    }

    public void promptUserToReplayGame(List<User> players) throws Exception {
        log.info("Would you like to play again? y to continue, any other key to exit.");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        if ("y".equalsIgnoreCase(br.readLine())) {
            startNewGame(players);
        } else {
            log.info("Thank you for playing.");
            br.close();
        }
    }

    public static void main(String[] args) throws Exception {
        ConsoleBasedPokerApplication consoleBasedPokerApplication = new ConsoleBasedPokerApplication();
        consoleBasedPokerApplication.start();
    }

    private List<User> getHumanPlayers() throws Exception {
        log.info("How many humans players? (Default is 1), 0 is also acceptable.");
        int humanUsersCount = 1;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            humanUsersCount = Integer.parseInt(br.readLine());
            if (humanUsersCount > 9) {
                log.info("Can't play with more than 9 players.");
                return getHumanPlayers();
            }
        } catch (NumberFormatException e) {
            // no need to catch as we have default value.
        }

        List<User> humanUsers = new ArrayList<>();

        for (int i = 0; i < humanUsersCount; i++) {
            log.info("Enter name of human user " + (i + 1));
            String name = br.readLine();
            if (name == null || "".equals(name)) {
                log.info("You didn't enter your name. Defaulting to name - Human");
                name = "Human";
            }
            User user = new User(name, INITIAL_BALANCE, false);
            humanUsers.add(user);
        }
        return humanUsers;
    }

    private List<User> getBotPlayers(int humanUsersCount) throws Exception {
        log.info("How many bot players? (Default is 1), 0 is also acceptable.");
        int botPlayers = 1;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            botPlayers = Integer.parseInt(br.readLine());
        } catch (NumberFormatException e) {
            // no need to catch as we have default value.
        }
        if (botPlayers + humanUsersCount > 9) {
            log.info("Can't play with more than 9 players. There are already " + humanUsersCount + " human players.");
            return getBotPlayers(humanUsersCount);
        }
        List<User> bots = new ArrayList<>();
        for (int i = 0; i < botPlayers; i++) {
            User user = new User("Bot " + (i + 1), INITIAL_BALANCE, true);
            bots.add(user);
        }
        return bots;
    }
}
