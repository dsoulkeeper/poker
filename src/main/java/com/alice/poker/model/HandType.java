package com.alice.poker.model;

import lombok.Getter;

@Getter
public enum HandType {
    HIGH_CARD(1L),

    ONE_PAIR(60L),// 59 is the high card max possible weight is HighCard(1) * ACE(14)+KING(13)+QUEEN(12)+JACK(11)+Nine(9)

    TWO_PAIRS(900L),// 876 is the one pair max possible weight: ONE_PAIR(60)xACE(14)+KING(13)+QUEEN(12)+JACK(11)

    THREE_OF_A_KIND(25000L),//24312 is the two pair max possible weight: TWO_PAIRS(900)xACE(14)+TWO_PAIRS(900)xKING(13)+QUEEN(12)

    STRAIGHT(350100L), //350025 is the three of a kind max possible weight: THREE_OF_A_KIND(25000)xACE(14)+KING(13)+QUEEN(14)

    FLUSH(21_006_100L),//21_006_000 is the straight max possible weight: STRAIGHT(350100)xSUM_OF(ACE(14)+KING(13)+QUEEN(12)+JACK(11)+TEN(10))

    FULL_HOUSE(1_239_360_000L),// 1_239_359_900 is the flush max possible weight: FLUSH(21_006_100) + ACE(14)+KING(13)+QUEEN(12)+JACK(11)+Nine(9)

    FOUR_OF_A_KIND(33_462_720_100L),// 33_462_720_000 is the full house max possible weight: FULL_HOUSE(1_239_360_000)xACE(14) + FULL_HOUSE(1_239_360_000)xKING(13)

    STRAIGHT_FLUSH(468_478_081_413L); //  468_478_081_413 is the four of a kind max possible weight: FOUR_OF_A_KIND(33_462_720_100) x ACE(14) + KING(13)

    // Royal flush is basically a highest STRAIGHT_FLUSH, hence not in the list

    // Significance of weight is to determine superiority of the hands.
    // Weights are super useful when there is a clash between same types of hands e.g. 2 full houses or 2 flushes.
    private Long weight;

    HandType(Long weight) {
        this.weight = weight;
    }
}