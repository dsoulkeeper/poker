package com.alice.poker.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User {

    private final String name;
    private Double balance;
    private boolean isBot;
}
