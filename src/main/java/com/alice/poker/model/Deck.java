package com.alice.poker.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {

    private final List<Card> cards = new ArrayList<>();

    public Deck() {
        int id = 0;
        for (Rank rank : Rank.values()) {
            for (Suit suit : Suit.values()) {
                cards.add(new Card(id++, suit, rank, false));
            }
        }
    }

    public List<Card> getCards() {
        return Collections.unmodifiableList(cards);
    }

    public Card getCard(int index) {
        return cards.get(index);
    }
}
