package com.alice.poker.model;

public enum Suit {
    HEART, DIAMOND, SPADE, CLUB
}
