package com.alice.poker.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PokerHand {
    private List<Card> flopCards;
    private Card turnCard;
    private Card riverCard;


    private List<PlayerPokerHand> playerPokerHands = new ArrayList<>();

    private Deck deck = new Deck();

    private Double bigBlind = 10D;
    private Bet lastBet;

    public List<Card> getCommunityCards() {
        ArrayList<Card> cards = new ArrayList<>(flopCards);
        cards.add(turnCard);
        cards.add(riverCard);
        return cards;
    }

    public Double calculateWinningAmount() {
        return playerPokerHands.stream()
                .mapToDouble(PlayerPokerHand::getTotalBetAmount)
                .sum();
    }
}
