package com.alice.poker.model;

public enum Round {
    PREFLOP, FLOP, TURN, RIVER
}
