package com.alice.poker.model;

import lombok.Data;

@Data
public class Bet {

    private Double amount;
    private Round round;
    private BetType betType;
    private PlayerPokerHand playerPokerHand;
    // this bet represents previous bet made by previous player
    private Bet previousBet;
}
