package com.alice.poker.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Card {
    private final int id;
    private final Suit suit;
    private final Rank rank;
    private boolean used;

    @Override
    public String toString() {
        return rank + " of " + suit;
    }
}
