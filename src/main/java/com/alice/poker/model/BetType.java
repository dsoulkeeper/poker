package com.alice.poker.model;

public enum BetType {
    CHECK, FOLD, CALL, RAISE, ALL_IN
}
