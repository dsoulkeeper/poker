package com.alice.poker.model;

import com.alice.poker.service.BettingService;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CollectBetContext {
    private final PlayerPokerHand playerPokerHand;
    private final PokerHand pokerHand;
    private final Bet lastPlayerBet;
    private final Round round;
    private final BettingService bettingService;
}
