package com.alice.poker.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

// this pojo represents a user who is holding 2 hole cards in the hand being played
@Data
public class PlayerPokerHand {

    private final User user;

    private List<Card> holeCards;

    private List<Card> bestCombination;

    private Long weight;

    private HandType handType;

    private List<Bet> preFlopBets = new ArrayList<>();
    private List<Bet> flopBets = new ArrayList<>();
    private List<Bet> turnBets = new ArrayList<>();
    private List<Bet> riverBets = new ArrayList<>();

    private boolean folded;
    private boolean allIn;

    boolean hasBetPreFloped() {
        return !preFlopBets.isEmpty();
    }

    boolean hasBetFlop() {
        return !flopBets.isEmpty();
    }

    boolean hasBetTrun() {
        return !turnBets.isEmpty();
    }

    boolean hasBetRiver() {
        return !riverBets.isEmpty();
    }

    public boolean hasBetRound(Round round) {
        if (round.equals(Round.PREFLOP)) {
            return hasBetPreFloped();
        } else if (round.equals(Round.FLOP)) {
            return hasBetFlop();
        } else if (round.equals(Round.TURN)) {
            return hasBetTrun();
        } else if (round.equals(Round.RIVER)) {
            return hasBetRiver();
        }
        return false;
    }

    public Double getTotalBetAmount() {
        List<Bet> allBets = new ArrayList<>(preFlopBets);
        allBets.addAll(flopBets);
        allBets.addAll(turnBets);
        allBets.addAll(riverBets);

        return allBets.stream().filter(bet -> bet.getAmount() != null)
                .mapToDouble(Bet::getAmount)
                .sum();
    }
}
